const mongoLib  = require('../libs/mongoLib')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const ensureAuth = require('../utils/ensureAuth')
const ObjectId = require('mongodb').ObjectId;
require('dotenv').config


myMongo = new mongoLib()

function routes(app){
    app.get('/', async (req,res)=>{
        res.send('ok')
    })

    app.get('/artists', async (req,res)=>{
        
        let response
        if (req.query.id == 'all'){
            response = await myMongo.get({},'artistas')
        }
        else{
            response = await myMongo.get({_id:req.query.id}, 'artistas')
        }
       
       res.json(response)
    })
    app.get('/artists/destacados', async (req,res)=>{
        let response = await myMongo.get({},'artistas') // enviar solo 4
        res.json(response)
    })

    app.get('/works', async (req,res) =>{
        console.log(req.query.id)
        let response = await myMongo.get({autor:ObjectId(req.query.id), index: {$ne:null}},'obras')
        console.log(response)
        res.json(response)
    })

    app.get('/news', async (req,res) =>{
        let response = await myMongo.get({},'news',null,{date:-1},Number(req.query.qty))
        res.json(response)
    })

    app.get('/newsbyid', async (req,res)=>{
        let response = await myMongo.get({_id:req.query.id}, 'news')
        res.json(response)
    })

    /*----------------------------------------------------------------------------*/
    /*----------------------------- Private area ---------------------------------*/
    /*----------------------------------------------------------------------------*/

    app.get('/login',async (req,res) =>{

        let user_in_database = await myMongo.get({user: req.query.user}, 'artistas')
        if(!user_in_database[0]){
            res.json({status: 0, msg:'usuario no encontrado'})
        }else{
            bcrypt.compare(req.query.password, user_in_database[0].pass, (err,result) =>{
                if(result){
                    let token = jwt.sign({user:user_in_database[0].user}, process.env.JWTSECRET)
                    res.json({status:2, token})
                }else{
                    res.json({status: 1, msg: 'clave incorrecta'})
                }
            })
        }
    })

    app.get('/artistinfo', ensureAuth, async (req,res) =>{
        let response = await myMongo.get({user:req.user},'artistas')
        res.json(response[0])
    })

    app.put('/changeprofileimage', ensureAuth, async (req,res)=>{
        let response = await myMongo.update({user:req.user},{$set:{image:req.body.image}},'artistas')
        res.json({status:response.modifiedCount})
    })

    app.post('/uploadwork', ensureAuth, async (req,res) =>{
        
        let response = await myMongo.get({user:req.user},'artistas', {_id:1})
        let autor = response[0]._id

        response = await myMongo.create({
            titulo: req.body.titulo,
            link: req.body.image,
            descripcion: req.body.desc,
            index: null,
            autor
        },'obras')
        if(response != -1){
            res.json({status:1})
        }
    })

    app.get('/allworks', ensureAuth, async (req,res) =>{
        let response = await myMongo.get({user: req.user},'artistas',{_id: 1})
        let autor = response[0]._id

        response = await myMongo.get({autor:autor}, 'obras')
        res.json(response)
    })

    app.put('/updateindex', ensureAuth,(req,res) =>{
        
        req.body.forEach(work =>{
            myMongo.update({_id: work._id}, {$set: {index: work.index}}, 'obras')
        })
        res.json({recived:1})
    })

    app.put('/updatework', ensureAuth, async (req,res) =>{

        let response = await myMongo.update(
            {_id: req.body._id},
            {$set: {titulo: req.body.title, descripcion: req.body.desc}
        },'obras')
        res.json({status: response.modifiedCount})
    })

    app.put('/updateinfo', ensureAuth, async (req,res) =>{
        let response = await myMongo.update({user: req.user},{$set:{name:req.body.name, desc:req.body.desc}},'artistas')
        res.json({status: response.modifiedCount}) 
    })
}

module.exports = routes