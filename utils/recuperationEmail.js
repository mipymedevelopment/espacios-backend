const nodemailer = require("nodemailer");
require('dotenv').config


async function sendRecuperationEmail(Email, newPass) {
  
    var transporter = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: process.env.GMAIL_ACCOUNT,
            pass: process.env.GMAIL_PASSWORD
        }
    });

    const html = `
            <h1>Correo de recuperación</h1>
            <p>Te enviamos un correo con tu nueva clave. Tu nueva contraseña es <h3>${newPass}</h3></p>
            <p>Exito en todos tus proyectos, mipymeTeam</p>
            `
    const text = `Correo de recuperacion. Te enviamos un correo con tu nueva clave. Tu nueva contraseña es ${newPass}. Exito en todos tus proyectos, mipymeTeam.`

    let info = await transporter.sendMail({
        from: process.env.GMAIL_ACCOUNT, 
        to: Email, 
        subject: "JazzDelivery: Correo de recuperacion", 
        text: text, 
        html: html, 
    });

    console.log("Message sent: %s", info.messageId);
    console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
}


module.exports = sendRecuperationEmail