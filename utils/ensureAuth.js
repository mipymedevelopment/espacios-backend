const jwt = require('jsonwebtoken')
require('dotenv').config

module.exports = function (req, res, next) {

  if (!req.headers.authorization) {
    return res
      .status(401)
      .json({status:0})
  }

  var token = req.headers.authorization.split(" ")[1];
  jwt.verify(token,process.env.JWTSECRET, (err,decoded) =>{
    if(decoded){
      req.user = decoded.user
      next()
    }else{
      res.status(401).json({status:0})
    }
  })
  
}