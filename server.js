
const express = require('express')
var bodyParser = require('body-parser')
const cors = require('cors')

const routes = require('./routes/routes.js')

const app = express()

app.use(bodyParser.json({limit:'50mb'}))
app.use(cors())
/*-------------------------------------------------------------------*/
/*---------------- Express ------------------------------------------*/
/*-------------------------------------------------------------------*/

routes(app)

app.listen(process.env.PORT || 4001)