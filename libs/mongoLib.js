const MongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectId;
require('dotenv').config()


const URI = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASSWORD}@cluster0.7qk4l.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`

class MongoLib{
  constructor(){
    this.client = new MongoClient(URI,{ useUnifiedTopology: true })
    this.db_name = 'espacios-publicos'
  }
  //--------------------------------------Connect
  connect(){
    if(!this.connection){
      this.connection = new Promise((resolve,reject) =>{
        this.client.connect(err=>{
          if(err){
            reject(err)
          }else{
            console.log('ok db connected')
            resolve()
          }
        })
      })
    }

    return this.connection
  }
  
  //--------------------------------------get
  async get(params,collection='inventario', projection=null, sort=null, limit=0){
    if(params._id){
      params._id = ObjectId(params._id)
    }
    try
    {
      await this.connect()
      const db = this.client.db(this.db_name)
      const r = await db.collection(collection).find(params,{sort}).project(projection).limit(limit).toArray()
      return r
    }catch(error)
    {
      console.log(error)
      return -1
    }
  }
  //--------------------------------------create
  async create(item,collection='inventario'){

    try{
      await this.connect()
      const db = this.client.db(this.db_name)
      const r = await db.collection(collection).insertOne(item)
      return r.insertedId

    }catch(err){
      console.log(err)
      return -1
    }
  }
  //--------------------------------------update
  async update(query, document, collection='inventario', arrayFilters_=false){
    
    if(query._id){
      query._id = ObjectId(query._id)
    }
    
    try{

      if(arrayFilters_){
        await this.connect()
        const db = this.client.db(this.db_name)
        const r = await db.collection(collection).updateOne(query, document, {arrayFilters: arrayFilters_})
        return r
      }else{
        await this.connect()
        const db = this.client.db(this.db_name)
        const r = await db.collection(collection).updateOne(query, document)
        return r
      }

      

    }catch(err){
      console.log(err)
      return -1
    }
  }
  //--------------------------------------update many
  async updateMany(query, document, collection='inventario'){
    
    if(query._id){
      query._id = ObjectId(query._id)
    }
    
    try{
      await this.connect()
      const db = this.client.db(this.db_name)
      const r = await db.collection(collection).updateMany(query, document)
      return r
    }catch(err){
      console.log(err)
      return -1
    }
  }
  //--------------------------------------delete
  async delete(query,collection='inventario'){
    
    if(query._id){
      query._id = ObjectId(query._id)
    }
    
    try{
      await this.connect()
      const db = this.client.db(this.db_name)
      const r = await db.collection(collection).deleteMany(query)
      return r
    }catch(err){
      console.log(err)
      return -1
    }
  }
}

module.exports = MongoLib
